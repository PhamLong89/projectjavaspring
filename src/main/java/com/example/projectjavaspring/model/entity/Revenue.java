package com.example.projectjavaspring.model.entity;

import java.time.LocalDate;
import lombok.Data;

@Data
public class Revenue {

  private LocalDate date;
  private Long revenue;
}
