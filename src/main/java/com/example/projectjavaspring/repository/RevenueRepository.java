package com.example.projectjavaspring.repository;

import com.example.projectjavaspring.model.entity.Order;
import com.example.projectjavaspring.model.entity.Revenue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface RevenueRepository extends JpaRepository<Order, Integer> {

  @Query(value = "SELECT o.orderDate as date, o.orderNumberPhone as revenue FROM OrderDetail od JOIN od.order o WHERE month(o.orderDate) = 8 GROUP BY o.orderDate ORDER BY o.orderDate")
  Revenue getRevenueByDate();

}
